<?php

namespace FpDbTest\QueryParser;

use Exception;
use FpDbTest\QueryBuilder\ArgumentBuilder;
use FpDbTest\QueryBuilder\ConditionalBlockBuilder;

class SimpleQueryParser implements QueryParser
{
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function parse(string $query): array
    {
        $len = strlen($query);
        $result = [];
        $blockStart = null;
        $argumentNumber = 0;
        $builders = [];
        $char = '';
        $skip = false;
        $endComment = '';
        for ($position = 0; $position < $len; $position++) {
            $last = sprintf('%s%s', $char, $query[$position]);
            switch ($last) {
                case '/*':
                    $skip = true;
                    $endComment = '*/';
                    break;
                case '--':
                    $skip = true;
                    $endComment = "\n";
                    break;
                case $endComment:
                    $skip = false;
                    break;
            }
            if ($skip) {
                continue;
            }
            $char = $query[$position];
            switch ($char) {
                case '{':
                    if (!is_null($blockStart)) {
                        throw new Exception('Block can not contains blocks');
                    }
                    $blockStart = $position;
                    break;
                case '}':
                    if (is_null($blockStart)) {
                        throw new Exception(sprintf('Unexpected block closed brace in %d position', $position));
                    }
                    $result[] = new ConditionalBlockBuilder($blockStart, $position - $blockStart + 1, ...$builders);
                    $blockStart = null;
                    $builders = [];
                    break;
                case '?':
                    $format = $query[$position + 1] ?? null;
                    if (!in_array($format, ['#', 'd', 'f', 'a'], true)) {
                        $format = null;
                    }
                    if (is_null($blockStart)) {
                        $result[] = new ArgumentBuilder($argumentNumber, $position, $format);
                    } else {
                        $builders[] = new ArgumentBuilder(
                            $argumentNumber,
                            $position - $blockStart - 1,
                            $format
                        );
                    }
                    $argumentNumber++;
                    break;
            }
        }
        if (!is_null($blockStart)) {
            throw new Exception('Block not closed');
        }
        return $result;
    }
}
