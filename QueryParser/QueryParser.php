<?php

namespace FpDbTest\QueryParser;

use FpDbTest\QueryBuilder\Builder;

interface QueryParser
{
    /**
     * @param string $query
     * @return Builder[]
     */
    public function parse(string $query): array;
}
