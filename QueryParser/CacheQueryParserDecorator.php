<?php

namespace FpDbTest\QueryParser;

use FpDbTest\QueryBuilder\Builder;

class CacheQueryParserDecorator implements QueryParser
{
    private QueryParser $queryParser;
    /** @var array<string, Builder[]> */
    private array $cache = [];

    public function __construct(QueryParser $queryParser)
    {
        $this->queryParser = $queryParser;
    }

    /**
     * @inheritDoc
     */
    public function parse(string $query): array
    {
        if (!array_key_exists($query, $this->cache)) {
            $this->cache[$query] = $this->queryParser->parse($query);
        }
        return $this->cache[$query];
    }
}
