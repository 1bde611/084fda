<?php

namespace FpDbTest\Skipper;

class SpecialObjectSkipper implements Skipper
{
    private ?SkippedArgument $skippedArgument = null;

    /**
     * @inheritDoc
     */
    public function getSkippedArgument(): SkippedArgument
    {
        if (is_null($this->skippedArgument)) {
            $this->skippedArgument = new SkippedArgument();
        }
        return $this->skippedArgument;
    }

    /**
     * @inheritDoc
     */
    public function isArgumentSkipped(mixed $argument): bool
    {
        return $argument instanceof SkippedArgument;
    }
}
