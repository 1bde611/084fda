<?php

namespace FpDbTest\Skipper;

/**
 * Скиппер. Утилита для работы со специальным значением пропуска условного блока
 */
interface Skipper
{
    /**
     * Возвращает специальное значение для пропуска условного блока
     *
     * @return mixed
     */
    public function getSkippedArgument(): mixed;

    /**
     * Проверяет передаваемое значение со специальным
     *
     * @param mixed $argument
     * @return bool
     */
    public function isArgumentSkipped(mixed $argument): bool;
}
