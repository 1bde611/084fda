# Процесс выполнения задачи

## Шаг 1

1. Сначала я определил интерфейс `\FpDbTest\Skipper\Skipper`. Интерфейс предоставляет два метода:
    - `getSkippedArgument(): mixed` - Возвращает специальное значение для пропуска условного блока;
    - `isArgumentSkipped(mixed $argument): bool` - Проверяет передаваемое значение со специальным.

2. Далее был определён интерфейс `\FpDbTest\QueryBuilder\Builder` с одним единственным методом:
    - `build(\FpDbTest\Skipper\Skipper $skipper, string $query, array $args): string` - Строит запрос из скиппера, шаблона и аргументов.

3. Скиппер и Сборщик добавлены в зависимости класса `\FpDbTest\Database`. 
Эти параметры конструктора опциональны для обратной совместимости. Сейчас, если не передать хотя бы один из них,
при попытке построить запрос произойдёт ошибка. В последующем им будут присвоены дефолтные значения.

## Шаг 2

1. В качестве специального значения я решил использовать отдельный класс `\FpDbTest\Skipper\SkippedArgument`.
Это пустой класс, но проверка по классу будет работать лучше, чем проверка на какую-либо строку или другое константное значение.
Хотя бы потому, что это константное значение может каким-то случайным образом оказаться именно значением аргумента и условный блок с ним не должен быть пропущен.

2. Написана реализация Скиппера `\FpDbTest\Skipper\SpecialObjectSkipper`, использующая `\FpDbTest\Skipper\SkippedArgument`.

3. В конструкторе класса `\FpDbTest\Database` по умолчанию используется новый скиппер.

## Шаг 3

1. Теперь я определил интерфейс `\FpDbTest\QueryParser\QueryParser` с методом:
    - `public function parse(string $query): array` - Возвращает массив сборщиков запроса.

2. Парсер позволит применить паттерн "композиция" и раскидать логику по классам. Именно это я и реализовал
в классе `\FpDbTest\QueryBuilder\QueryBuilder`

## Шаг 4

1. Добавил класс `\FpDbTest\QueryBuilder\ArgumentBuilder` - сборщик запроса, который берёт конкретный аргумент из
всего списка, приводит его к заданному формату и заменяет в шаблоне запроса подстроку для этого аргумента.

2. Добавил класс `\FpDbTest\QueryBuilder\ConditionalBlockBuilder` - сборщик запроса, который обрабатывает условный блок. 
Если необходимый аргумент определяется скиппером, как специальный, блок удаляется из итогового запроса, иначе в нём
необходимые аргументы заменяются с помощью массива `\FpDbTest\QueryBuilder\ArgumentBuilder`.

3. Реализован `\FpDbTest\QueryParser\SimpleQueryParser` - парсер, который разбирает шаблон запроса и возвращает 
массив сборщиков запроса (`\FpDbTest\QueryBuilder\ArgumentBuilder` или `\FpDbTest\QueryBuilder\ConditionalBlockBuilder`)

4. В конструкторе класса `\FpDbTest\Database` сборщик по умолчанию определяется, как `\FpDbTest\QueryBuilder\QueryBuilder`,
использующий новый парсер.

## Шаг 5

В целом, уже работает, но можно сделать чуть лучше. 
Часто запросы повторяются (меняются лишь параметры), и это толкает на мысль закэшировать
результаты парсера запросов хотя бы в памяти. Но если работать через RoadRunner или что-то похожее, 
то InMemory кэша в принципе будет достаточно.

1. Добавлен кэширующий декоратор `\FpDbTest\QueryParser\CacheQueryParserDecorator` парсера запроса.

2. В конструкторе класса `\FpDbTest\Database` по умолчанию используется кэширующий декоратор парсера.

---

На этом с меня хватит =)

Но улучшать можно и дальше. Например (навскидку):
- задекорировать парсер так, чтобы сначала из запроса удалялись комментарии;
- прикрутить настоящий кэш парсера (PSR-6 / PSR-16 / что-то ещё);
- написать сборщик и парсер, умеющие работать со вложенными условными блоками.

Ну и конечно же, в следующем мажорном релизе параметры конструктора класса `\FpDbTest\Database` сделать обязательными.