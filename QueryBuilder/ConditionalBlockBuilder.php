<?php

namespace FpDbTest\QueryBuilder;

use FpDbTest\Skipper\Skipper;

class ConditionalBlockBuilder implements Builder
{
    private int $position;
    private int $len;
    /** @var ArgumentBuilder[] */
    private array $argumentReplacers;
    private array $argumentNumbers;

    public function __construct(
        int $position,
        int $len,
        ArgumentBuilder $argumentReplacer,
        ArgumentBuilder ...$additionalArgumentReplacers)
    {
        $this->position = $position;
        $this->len = $len;
        array_unshift($additionalArgumentReplacers, $argumentReplacer);
        krsort($additionalArgumentReplacers);
        $this->argumentReplacers = $additionalArgumentReplacers;
        foreach ($this->argumentReplacers as $argumentReplacer) {
            $this->argumentNumbers[$argumentReplacer->argumentNumber] = $argumentReplacer->argumentNumber;
        }
    }

    /**
     * @inheritDoc
     */
    public function build(Skipper $skipper, string $query, array $args): string
    {
        $replacement = $this->buildBlock($skipper, $query, $args);
        return substr_replace($query, $replacement, $this->position, $this->len);
    }

    private function buildBlock(Skipper $skipper, string $source, array $args): string
    {
        foreach ($this->argumentNumbers as $argumentNumber) {
            if ($skipper->isArgumentSkipped($args[$argumentNumber])) {
                return '';
            }
        }

        $block = substr($source, $this->position + 1, $this->len - 2);
        foreach ($this->argumentReplacers as $argumentReplacer) {
            $block = $argumentReplacer->build($skipper, $block, $args);
        }
        return $block;
    }
}
