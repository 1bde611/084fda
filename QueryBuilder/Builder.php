<?php

namespace FpDbTest\QueryBuilder;

use FpDbTest\Skipper\Skipper;

interface Builder
{
    /**
     * Строит запрос из скиппера, шаблона и аргументов.
     *
     * @param Skipper $skipper
     * @param string $query
     * @param array<array-key, mixed> $args
     * @return string
     */
    public function build(Skipper $skipper, string $query, array $args): string;
}
