<?php

namespace FpDbTest\QueryBuilder;

use FpDbTest\QueryParser\QueryParser;
use FpDbTest\Skipper\Skipper;

class QueryBuilder implements Builder
{
    public function __construct(
        private readonly QueryParser $queryParser,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function build(Skipper $skipper, string $query, array $args): string
    {
        // Гарантируем, что все значения аргументов будут идти по порядку
        $args = array_values($args);
        // Получаем частичные сборщики запроса
        $builders = $this->queryParser->parse($query);
        // Меняем в обратном порядке, чтобы проще было работать с позицией замены
        krsort($builders);

        $result = $query;
        // Собираем запрос
        foreach ($builders as $builder) {
            $result = $builder->build($skipper, $result, $args);
        }
        return $result;
    }
}
