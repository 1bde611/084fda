<?php

namespace FpDbTest\QueryBuilder;

use FpDbTest\Skipper\Skipper;
use TypeError;

class ArgumentBuilder implements Builder
{
    public function __construct(
        public readonly int $argumentNumber,
        private readonly int $position,
        private readonly ?string $format
    ) {
    }

    /**
     * @inheritDoc
     */
    public function build(Skipper $skipper, string $query, array $args): string
    {
        $replacement = $this->buildArgument($this->format, $args[$this->argumentNumber]);
        return substr_replace($query, $replacement, $this->position, is_null($this->format) ? 1 : 2);
    }

    private function buildArgument(?string $format, mixed $argument): string
    {
        return match ($format) {
            '#' => $this->buildId($argument),
            'd' => $this->buildInt($argument),
            'f' => $this->buildFloat($argument),
            'a' => $this->buildArray($argument, false),
            default => $this->buildAuto($argument),
        };
    }

    private function buildAuto(mixed $argument, bool $isArraysAllowed = true): string
    {
        if (is_null($argument)) {
            return 'NULL';
        }
        if (is_array($argument)) {
            if (!$isArraysAllowed) {
                throw new TypeError('Array items can not be an array.');
            }
            return $this->buildArray($argument, false);
        }
        if (is_int($argument)) {
            return $this->buildInt($argument);
        }
        if (is_float($argument)) {
            return $this->buildFloat($argument);
        }
        if (is_string($argument)) {
            return sprintf('\'%s\'', str_replace('\'', '\\\'', $argument));
        }
        throw new TypeError('Argument must be integer, float, boolean, string, array or null.');
    }

    private function buildArray(mixed $array, bool $isArraysAllowed = true): string
    {
        if (!is_array($array)) {
            throw new TypeError('Argument is not an array.');
        }
        $isList = array_keys($array) === range(0, count($array) - 1);
        $isFirst = true;
        $result = '';
        foreach ($array as $key => $value) {
            if ($isList) {
                $append = $this->buildAuto($value, $isArraysAllowed);
            } else {
                $append = sprintf('%s = %s', $this->buildId((string)$key), $this->buildAuto($value, $isArraysAllowed));
            }
            if ($isFirst) {
                $result = $append;
                $isFirst = false;
            } else {
                $result = sprintf('%s, %s', $result, $append);
            }
        }
        return $result;
    }

    private function buildId(mixed $id): string
    {
        if (is_array($id)) {
            return $this->buildIdArray($id);
        }
        return $this->buildIdValue($id);
    }

    private function buildIdValue(mixed $id): string
    {
        if (!is_string($id)) {
            throw new TypeError('Id is not a string.');
        }
        return sprintf('`%s`', $id);
    }

    private function buildIdArray(array $id): string
    {
        $isFirst = true;
        $result = '';
        foreach ($id as $value) {
            if ($isFirst) {
                $result = $this->buildIdValue($value);
                $isFirst = false;
                continue;
            }
            $result = sprintf('%s, %s', $result, $this->buildIdValue($value));
        }
        return $result;
    }

    private function buildInt(mixed $number): string
    {
        if (is_null($number)) {
            return 'NULL';
        }
        return sprintf('%d', $number);
    }

    private function buildFloat(mixed $number): string
    {
        if (is_null($number)) {
            return 'NULL';
        }
        return sprintf('%f', $number);
    }
}
