<?php

namespace FpDbTest;

use Exception;
use FpDbTest\QueryBuilder\Builder;
use FpDbTest\QueryBuilder\QueryBuilder;
use FpDbTest\QueryParser\CacheQueryParserDecorator;
use FpDbTest\QueryParser\SimpleQueryParser;
use FpDbTest\Skipper\Skipper;
use FpDbTest\Skipper\SpecialObjectSkipper;
use mysqli;

class Database implements DatabaseInterface
{
    private mysqli $mysqli;
    private ?Builder $builder;
    private Skipper $skipper;

    public function __construct(mysqli $mysqli, ?Builder $builder = null, ?Skipper $skipper = null)
    {
        $this->mysqli = $mysqli;
        $this->builder = $builder ?? new QueryBuilder(new CacheQueryParserDecorator(new SimpleQueryParser()));
        $this->skipper = $skipper ?? new SpecialObjectSkipper();
    }

    public function buildQuery(string $query, array $args = []): string
    {
        if (is_null($this->builder)) {
            throw new Exception();
        }
        return $this->builder->build($this->skipper, $query, $args);
    }

    public function skip()
    {
        return $this->skipper->getSkippedArgument();
    }
}
